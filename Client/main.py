import requests
from PIL import Image

adresse = "http://127.0.0.1:8000/"

nombredecarte = 10

#creation deck
req = requests.get(adresse+"creer-un-deck")
id = req.json()["deck_id"]

print("id = "+id)

#tirage des cartes
data_json = {"nombre_cartes": nombredecarte}
req = requests.post(adresse+"cartes", json=data_json)
res = req.json()

cards = res["cards"]

#comptage des cartes

def comptage(cards):
    count = {"HEARTS":0,"SPADES":0,"DIAMONDS":0,"CLUBS":0}

    for c in cards:
        count[c["suit"]] += 1

    return count
print("\nNombre de carte")
print(comptage(cards))


#affichage des cartes
def get_concat_h(im1, im2):
    dst = Image.new('RGB', (im1.width + im2.width, im1.height))
    dst.paste(im1, (0, 0))
    dst.paste(im2, (im1.width, 0))
    return dst

im = []
for c in cards:
    url = c["images"]["png"]
    im.append(Image.open(requests.get(url, stream=True).raw))

if cards != []:
    dst = im[0]
    for i in range(1,len(im)):
        dst = get_concat_h(dst, im[i])
    dst.show()

