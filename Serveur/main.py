from fastapi import FastAPI

from metier import DrawAction

import requests

app = FastAPI()

id = ""

def deckCreation():
    global id
    req = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
    json = req.json()

    id = json["deck_id"]

@app.get("/")
def readRoot():
    return {"API utilisée: https://deckofcardsapi.com/"}

@app.get("/creer-un-deck")
def createDeck():

    deckCreation()

    return {"deck_id":id}

@app.post("/cartes")
def DrawCards(da:DrawAction):
    
    deck_created = False

    if (id == ""):
        deckCreation()
        deck_created = True

    req = requests.get("https://deckofcardsapi.com/api/deck/"+id+"/draw/?count="+str(da.nombre_cartes))
    json = req.json()

    if "error" in json:
        res = {"deck_id": id, "deck_created": deck_created, "cards": json["cards"], "error": json["error"]}
    else:
        res = {"deck_id": id, "deck_created": deck_created, "cards": json["cards"]}

    return res
